<?php 
    require_once('config.php'); // database connection file
    //for value set 
    $id = $_REQUEST['update_id'];
    $slt="SELECT * FROM std_info NATURAL JOIN std_dept WHERE id='$id'";
    $result=mysqli_query($connect,$slt);
    $info=mysqli_fetch_array($result);
    //end of value set

    //!empty($_POST)
    if(isset($_POST['form2'])){
      $ID = $_REQUEST['update_id'];
      $name= $_POST['name'];
      $roll= $_POST['roll'];
      $email= $_POST['email'];
      $cell= $_POST['cell'];
      $dept_id= $_POST['dept_id'];
      $session= $_POST['session'];
      $picture= $_FILES['picture'];
      $picture_name ='Stuent-'.time().'-'.rand(1000,100000).'.'.pathinfo($picture['name'],PATHINFO_EXTENSION);
      
      //mysql query
      if(!empty($name)){
        $update = "UPDATE std_info SET name='$name', roll='$roll', email='$email', cell='$cell', dept_id='$dept_id', session='$session', picture='$picture_name' WHERE id='$ID' ";
        $update_query = mysqli_query($connect,$update); //insert query
        //innser if else or nested if else
        if( $update_query){
            move_uploaded_file($picture['tmp_name'],'img/'.$picture_name);
            echo "Data has ben Successfully Updated";
            header("Location:index.php");
        }else{
          echo "Data Update Error!";
        }//End of the inner if else 

      }else{
        echo "Name Please";  
    }
  }
?>
<!doctype html>
<html  lang="en-US">
    <head>
        <title>Update Student Information</title>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- all css -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
    <!-- all content goes here -->
    <div class="wrap">
      <div class="container">
        <div class="row">
          <h1 class="text-center">Update Student Information</h1>
          <div class="col-md-6 col-md-offset-3">
          <!-- form -->
              <form action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" id="name" name="name" value="<?= $info['name']; ?>" class="form-control" />
                </div>
                <div class="form-group">
                  <label for="roll">Roll</label>
                  <input type="text" id="roll" name="roll" value="<?= $info['roll']; ?>" class="form-control" />
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" id="email" name="email" value="<?= $info['email']; ?>" class="form-control" />
                </div>
                <div class="form-group">
                  <label for="cell">Phone</label>
                  <input type="text" id="cell" name="cell" value="<?= $info['cell']; ?>" class="form-control" />
                </div>
                <div class="from-group">
                  <label for="dept">Department</label>
                  <select class="form-control" name="dept_id" id="dept">
                  <!--   <option value="">Select Department</option> -->
                      <?php 
                        $sl="SELECT * FROM std_dept ORDER BY dept_name";
                        $qr=mysqli_query($connect,$sl);
                        while($dept=mysqli_fetch_array($qr)){ ?>
                         <option value="<?= $dept['dept_id']; ?>"><?= $dept['dept_name']; ?></option>
                      <?php  }
                      ?>                  
                  </select>
                </div>
               <div class="from-group">
                  <label for="session">Session</label>
                  <input type="text" id="session" name="session" value="<?= $info['session']; ?>" class="form-control" />
              </div>
               <div class="form-group">
                 <label for="pp">Profiel Picture</label><br/>
                 <img width="100px" src="img/<?= $info['picture']; ?>" alt="">
                 <input type="file" id="pp"  name="picture" />
               </div> 
               <button type="submit" name="form2" class="btn btn-success">Update</button>
              </form>
              <!-- /form -->
          </div>
        </div>
      </div>
    </div>
    <!-- all js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>
