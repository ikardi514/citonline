<?php 
  require_once('config.php'); //db config file
  $id = $_REQUEST['view_id'];
  $slt = "SELECT * FROM std_info NATURAL JOIN std_dept WHERE id='$id'"; 
  $qry = mysqli_query($connect,$slt);
  $info=mysqli_fetch_array($qry);
?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Student Registration</title>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- all css -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
    <!-- all content goes here -->
    <div class="wrap">
      <div class="container">
        <div class="row">
          <h1 class="text-center">Student Information's</h1>
          <div class="col-md-8 col-md-offset-2">
          <!-- form -->
          <table class="table table-hover table-striped">
            <tr>
              <td>Name</td>
              <td><?= $info['name']; ?></td>
            </tr>
            <tr>
              <td>Roll</td>
              <td><?= $info['roll']; ?></td>
            </tr>
            <tr>
              <td>Email</td>
              <td><?= $info['email']; ?></td>
            </tr>
            <tr>
              <td>Phone</td>
              <td><?= $info['cell']; ?></td>
            </tr>
            <tr>
              <td>Department</td>
              <td><?= $info['dept_name']; ?></td>
            </tr>
            <tr>
              <td>Session</td>
              <td><?= $info['session']; ?></td>
            </tr>
            <tr>
              <td>Profile Pricture</td>
              <td>
                <img width="80px" src="img/<?= $info['picture']; ?>" alt="">
              </td>
            </tr>
          </table>
              <!-- /form -->
          </div>
        </div>
      </div>
    </div>
    <!-- all js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>
