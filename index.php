<?php 
    require_once('config.php'); 

    if(isset($_POST['form1'])){
      $name= $_POST['name'];
      $roll= $_POST['roll'];
      $email= $_POST['email'];
      $cell= $_POST['cell'];
      $dept_id= $_POST['dept_id'];
      $session= $_POST['session'];
      $picture= $_FILES['picture'];
      $picture_name ='Stuent-'.time().'-'.rand(1000,100000).'.'.pathinfo($picture['name'],PATHINFO_EXTENSION);
      
      //mysql query
      if(!empty($name)){

        $insert = "INSERT INTO std_info(id, name, roll, email, cell, dept_id, session, picture) VALUES('', '$name', '$roll', '$email', '$cell', '$dept_id', '$session', '$picture_name')";
        $insert_query = mysqli_query($connect,$insert); 
        echo "Registration complete";
        
        if($insert_query){
            move_uploaded_file($picture['tmp_name'],'img/'.$picture_name);
            echo "Data has ben Inserted Successfullt";
            header("Location:index.php");
        }else{
          echo "Data Insert Error!";
        }//End of the inner if else 

      }else{
        echo "Name Please";  
    }
  }
?>
<!doctype html>
<html  lang="en-US">
    <head>
        <title>Student Registration</title>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- all css -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
    <!-- all content goes here -->
    <div class="wrap">
      <div class="container">
        <div class="row">
          <h1 class="text-center">Student Registration Form</h1>
          <div class="col-md-6 col-md-offset-3">
          <!-- form -->
              <form action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" id="name" name="name" class="form-control" />
                </div>
                <div class="form-group">
                  <label for="roll">Roll</label>
                  <input type="text" id="roll" name="roll" class="form-control">
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" id="email" name="email" class="form-control" />
                </div>
                <div class="form-group">
                  <label for="cell">Phone</label>
                  <input type="text" id="cell" name="cell" class="form-control" />
                </div>
                <div class="from-group">
                  <label for="dept">Department</label>
                  <select class="form-control" name="dept_id" id="dept">
                    <option value="">Select Department</option>
                      <?php 
                        $sl="SELECT * FROM std_dept ORDER BY dept_name";
                        $qr=mysqli_query($connect,$sl);
                        while($dept=mysqli_fetch_array($qr)){ ?>
                         <option value="<?= $dept['dept_id']; ?>"><?= $dept['dept_name']; ?></option>
                      <?php  }
                      ?>                  
                  </select>
                </div>
               <div class="from-group">
                  <label for="session">Session</label>
                  <input type="text" id="session" name="session" class="form-control" />
              </div>
               <div class="form-group">
                 <label for="pp">Profiel Picture</label>
                 <input type="file" id="pp" name="picture" />
               </div> 

                <div class="from-gender">
                  <label for="session">Male</label>

                  <input type="checkbox" id="gender" name="gender" class="form-gender" />
            
              
                  <label for="session">Female</label>

                  <input type="checkbox" id="gender" name="gender" class="form-gender" />
              </div>
               <button type="submit" name="form1" class="btn btn-success">Registration</button>
              </form>

              <!-- /form -->
          </div>
        </div>
      </div>
    </div>
    <!-- all js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>
