<?php 
  require_once('config.php'); 
?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Student Registration</title>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- all css -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
    <!-- all content goes here -->
    <div class="wrap">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
        <h1>Manage All Student's Information 
        
            <table class="table table-striped">
              <tr>
                <th>Sr No.</th>
                <th>Name</th>
                <th>Roll</th>
                <th>Email</th>
                <th>Department</th>
                <th>Action</th>
              </tr>
              <?php 
                  // data view
                  $serial = 1;
                  $info = "SELECT * FROM std_info NATURAL JOIN std_dept ORDER BY id DESC LIMIT 0,10";
                  $query = mysqli_query($connect,$info);
                  while($data=mysqli_fetch_array($query)){ ?>
                      <tr>
                        <td><?= $serial++; ?></td>
                        <td><?= $data['name']; ?></td>
                        <td><?= $data['roll']; ?></td>
                        <td><?= $data['email']; ?></td>
                        <td><?= $data['dept_name']; ?></td>
                        <td>
                          <a href="view.php?view_id=<?= $data['id']; ?>"><button type="button" class="btn btn-success btn-xs">view</button></a>
                          <a href="update.php?update_id=<?= $data['id']; ?>"><button type="button" class="btn btn-success btn-xs">Update</button></a>
                          <a href="delete.php?delete_id=<?= $data['id']; ?>"><button type="button" class="btn btn-success btn-xs">Delete</button></a>
                        </td>
                      </tr>

                <?php  }
              ?>
            </table>
          </div>

        </div>
      </div>
    </div>
    <!-- all js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>
